<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id');
            $table->integer('unit_id');
            $table->string('name')->unique();
            $table->char('short_form', 5);
            $table->integer('prime_cost');
            $table->decimal('price', 10, 2);
            $table->decimal('price_vat', 10, 2);
            $table->decimal('surcharge', 10, 2);
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
