<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->integer('work_id');
            $table->string('type', 10);
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('prime_cost');
            $table->decimal('price', 10, 2);
            $table->decimal('price_vat', 10, 2);
            $table->decimal('surcharge', 10, 2);
            $table->decimal('units_per_square', 10, 2);
            $table->boolean('editable');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
