<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->string('project_name')->unique();
            $table->decimal('rooms_squares', 10, 2);
            $table->decimal('bath_squares', 10, 2);
            $table->integer('room_count');
            $table->decimal('sum', 10, 2);
            $table->decimal('sum_vat', 10, 2);
            $table->decimal('square_price', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
