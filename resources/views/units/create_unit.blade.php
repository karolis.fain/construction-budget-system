@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
    <div class="header bg-gradient-primary py-4 py-lg-3">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header border-0">
                        <h3 class="mb-0">{{ __('Create new unit') }}</h3>
                    </div>
                    <form method="POST" action="{{ route('unit.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                       class="form-control @error('name') is-invalid @enderror"
                                       name="name" value="{{ old('name') }}" required autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="short_form" class="col-md-4 col-form-label text-md-right">{{ __('Short form') }}</label>

                            <div class="col-md-6">
                                <input id="short_form" type="text"
                                       class="form-control @error('short_form') is-invalid @enderror"
                                       name="short_form" value="{{ old('short_form') }}" required>

                                @error('short_form')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category_id" class="col-md-4 col-form-label text-md-right">
                                {{ __('Active') }} <span class="red">*</span>
                            </label>

                            <div class="col-md-6">
                                <select id="active" class="select-style form-control @error('active') is-invalid @enderror"
                                        name="active" required>
                                    <option {{ old('active') ? 'selected' : '' }} value="1">{{ __('Active') }}</option>
                                    <option {{ old('active') ? '' : 'selected' }} value="0">{{ __('Inactive') }}</option>
                                </select>

                                @error('active')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="divider-form"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
