@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
    <div class="header bg-gradient-primary py-4 py-lg-3">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">{{ __('Units') }}</h3>
                        <div class="divider-form"></div>
                        <div class="row">
                            <div class="col pd-sides">
                                <form action="" method="GET">
                                    <div class="col-9 pd-sides active-purple-3 active-purple-4">
                                        <input class="form-control" type="text" value="{{ old('search') ? old('search') : $search }}"
                                               name="search" placeholder="{{ __('Search') }}" aria-label="Search"
                                        >
                                    </div>
                                </form>
                            </div>
                            <div class="col pd-sides mg-t-6">
                                <a class="btn btn-primary btn-sm float-right" href="{{ route('unit.create') }}">
                                    {{ __('Add new unit') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    @if (session('success'))
                        <div id="alert-success" class="alert alert-success mrl-10" role="alert">
                            {{ __(session('success')) }}
                        </div>
                    @elseif (session('fail'))
                        <div id="alert-warning" class="alert alert-warning mrl-10" role="alert">
                            {{ __(session('fail')) }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort" data-sort="name">{{ __('Name') }}</th>
                                <th scope="col" class="sort" data-sort="budget">{{ __('Short form') }}</th>
                                <th scope="col" class="sort" data-sort="status">{{ __('Status') }}</th>
                                <th scope="col" class="sort" data-sort="status">{{ __('Created at') }}</th>
                                <th scope="col" class="sort" data-sort="status">{{ __('Updated at') }}</th>
                                <th scope="col" class="text-center">{{ __('Actions') }}</th>
                            </tr>
                            </thead>
                            <tbody class="list">
                            @foreach($units as $unit)
                                <tr>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <div class="media-body">
                                                <span class="name mb-0 text-sm">{{ $unit->name }}</span>
                                            </div>
                                        </div>
                                    </th>
                                    <td class="budget">{{ $unit->short_form }}</td>
                                    <td>
                                      <span class="badge badge-dot mr-4">
                                          @if ($unit->active == true)
                                              <i class="bg-success"></i>
                                              <span class="status">{{ __('Active') }}</span>
                                          @else
                                              <i class="bg-danger"></i>
                                              <span class="status">{{ __('Inactive') }}</span>
                                          @endif
                                      </span>
                                    </td>
                                    <td class="budget">{{ $unit->created_at }}</td>
                                    <td class="budget">{{ $unit->updated_at }}</td>
                                    <td class="text-center">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v mg-t-10"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item"
                                                   href="{{ route('unit.edit', $unit) }}">
                                                    {{ __('Edit') }}
                                                </a>
                                                <a data-toggle="modal" onclick="deleteData({{$unit->id}}, '{{$unit->name}}')"
                                                   data-target="#DeleteModal" class="dropdown-item">
                                                    {{ __('Delete') }}
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <!-- Card footer -->
                    <div class="card-footer py-4">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-end mb-0">
                                {{ $units->appends(['search' => Request::get('search')])->links('vendor.pagination.bootstrap-4') }}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('modal')
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/components/script.js') }}"></script>
    <script>
        function deleteData(id, name) {
            let url = '{{ route("unit.destroy", ":id") }}'
            url = url.replace(':id', id)
            document.getElementById('deleteInfo').innerText = '{{ __('Deleting unit -')}}' + ' ' + name
            $("#deleteForm").attr('action', url)
        }
        function formSubmit() {
            $("#deleteForm").submit();
        }
    </script>
@endpush
