@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
    <div class="header bg-gradient-primary py-4 py-lg-3">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header border-0">
                        <h3 class="mb-0">{{ __('Create base plan') }}</h3>
                    </div>
                    <hr class="mg-t-0">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    <form action="{{ route('plan.store') }}" onkeydown="return event.key != 'Enter';" method="POST">
                        @csrf

                        <div class="form-group row">
                            <div class="col inline">
                                <label for="project_name" class="col-md-5 col-form-label-inline text-md-right">
                                    {{ __('Name') }} <span class="red">*</span>
                                </label>

                                <div class="col-md-6">
                                    <input id="project_name" type="text"
                                           class="form-control @error('project_name') is-invalid @enderror"
                                           name="project_name" value="{{ old('project_name') ? old('project_name') : $plan->project_name }}" required autofocus>

                                    @error('project_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col inline">
                                <label for="room_count" class="col-md-5 col-form-label-inline text-md-right">
                                    {{ __('Rooms count') }} <span class="red">*</span>
                                </label>

                                <div class="col-md-6">
                                    <input id="room_count" type="number"
                                           class="form-control @error('room_count') is-invalid @enderror"
                                           name="room_count" value="{{ old('room_count') ? old('room_count') : $plan->room_count }}" required>

                                    @error('room_count')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col inline">
                                <label for="rooms_squares" class="col-md-5 col-form-label-inline text-md-right">
                                    {{ __('Rooms squares') }} <span class="red">*</span>
                                </label>

                                <div class="col-md-6">
                                    <input id="rooms_squares" type="number" step="0.01"
                                           class="form-control @error('rooms_squares') is-invalid @enderror"
                                           name="rooms_squares" value="{{ old('rooms_squares') ? old('rooms_squares') : $plan->rooms_squares }}" required>

                                    @error('rooms_squares')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="col inline">
                                <label for="bath_squares" class="col-md-5 col-form-label-inline text-md-right">
                                    {{ __('Bath/WC squares') }} <span class="red">*</span>
                                </label>

                                <div class="col-md-6">
                                    <input id="bath_squares" type="number" step="0.01"
                                           class="form-control @error('bath_squares') is-invalid @enderror"
                                           name="bath_squares" value="{{ old('bath_squares') ? old('bath_squares') : $plan->bath_squares }}" required>

                                    @error('bath_squares')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div id="alert-warning" class="alert alert-warning mrl-10" style="display: none"></div>
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col" class="sort">{{ __('Nr.') }}</th>
                                    <th scope="col" class="sort">{{ __('Work/material name') }}</th>
                                    <th scope="col" class="sort">{{ __('Unit me.') }}</th>
                                    <th scope="col" class="sort">{{ __('Quantity') }}</th>
                                    <th scope="col" class="sort">{{ __('Work price') }}</th>
                                    <th scope="col" class="sort">{{ __('Un. price') }}</th>
                                    <th scope="col" class="sort">{{ __('Price') }}</th>
                                    <th scope="col" class="sort">{{ __('Sum') }}</th>
                                    <th scope="col" class="sort">{{ __('Actions') }}</th>
                                </tr>
                                </thead>
                                <tbody id="plan-table-content">
                                @foreach($catsWorksProducts as $categoryIndex => $categoryWorks)
                                    @include('partials.category_row', ['category' => $categoryIndex, 'categories' => $categories])

                                    @foreach($categoryWorks as $workIndex => $workProducts)
                                        @include('partials.work_row', ['work' => $works->get($workIndex), 'quantity' => $worksQuantity[$workIndex]])
                                        @foreach($workProducts as $product)
                                            @include('partials.product_add', ['product' => $product])
                                        @endforeach

                                        @include('partials.product_last_row', ['products' => $groupedProducts->get($workIndex) ?? [], 'usedProducts' => $workProducts, 'disabled' => count($workProducts) >= count($groupedProducts->get($workIndex) ?? [])])
                                    @endforeach

                                    @include('partials.work_last_row', ['works' => $groupedWorks->get($categoryIndex), 'usedWorks' => $categoryWorks, 'disabled' => count($categoryWorks) >= count($groupedWorks->get($categoryIndex))])
                                @endforeach

                                @include('partials.category_last_row', ['categories' => $categories, 'usedCategories' => $catsWorksProducts, 'disabled' => count($usedCategories) >= count($categories)])
                                </tbody>
                            </table>
                        </div>
                        <div class="divider-form"></div>
                        <div class="form-group row mb-0 mg-r-20">
                            <div class="col-md-5 offset-md-7 burlywood">
                                <label for="sum" class="col-md-6 col-form-label text-md-right">{{ __('Total sum, €:') }}</label>
                                <input readonly value="{{ old('sum') ? old('sum') : $plan->sum }}" class="masked-input" name="sum">
                            </div>
                        </div>
                        <div class="form-group row mb-0 mg-r-20">
                            <div class="col-md-5 offset-md-7 burlywood">
                                <label for="sum_vat" class="col-md-6 col-form-label text-md-right">{{ __('Total sum (VAT), €:') }}</label>
                                <input readonly value="{{ old('sum_vat') ? old('sum_vat') : $plan->sum_vat }}" class="masked-input" name="sum_vat">
                            </div>
                        </div>
                        <div class="form-group row mg-r-20">
                            <div class="col-md-5 offset-md-7 burlywood">
                                <label for="square_price" class="col-md-6 col-form-label text-md-right">1m<sup>2</sup> {{ __('price (VAT), €:') }}</label>
                                <input readonly value="{{ old('square_price') ? old('square_price') : $plan->square_price }}" class="masked-input" name="square_price">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-2 offset-md-10">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                        <div class="divider-form"></div>
                    </form>
                </div>
                <div class="divider-form"></div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/components/plan.js') }}"></script>
@endpush
