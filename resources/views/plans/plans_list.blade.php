@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
    <div class="header bg-gradient-primary py-4 py-lg-3">
        <div class="row">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0">
                        <h3 class="mb-0">{{ __('Plans') }}</h3>
                        <div class="divider-form"></div>
                        <div class="row">
                            <div class="col pd-sides">
                                <form action="" method="GET">
                                    <div class="col-md-9 pd-sides active-purple-3 active-purple-4">
                                        <input class="form-control" type="text" value="{{ old('search') ? old('search') : $search }}"
                                               name="search" placeholder="{{ __('Search') }}" aria-label="Search"
                                        >
                                    </div>
                                </form>
                            </div>
                            <div class="col-md col-xs-3 pd-sides">
                                <a class="btn btn-primary btn-sm float-right mg-t-6 m-l-5" href="{{ route('plan.base') }}">
                                    {{ __('Add new base plan') }}
                                </a>
                                <a class="btn btn-primary btn-sm float-right mg-t-6 m-l-5" href="{{ route('plan.create') }}">
                                    {{ __('Add new plan') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    @if (session('success'))
                        <div id="alert-success" class="alert alert-success mrl-10" role="alert">
                            {{ __(session('success')) }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort">{{ __('Name') }}</th>
                                <th scope="col" class="sort">{{ __('Rooms squares') }}</th>
                                <th scope="col" class="sort">{{ __('Bath/WC squares') }}</th>
                                <th scope="col" class="sort">{{ __('Rooms count') }}</th>
                                <th scope="col" class="sort">{{ __('Sum') }}</th>
                                <th scope="col" class="sort">{{ __('Sum (VAT)') }}</th>
                                <th scope="col" class="sort">1m<sup>2</sup> {{ __('price') }}</th>
                                <th scope="col" class="sort">{{ __('Updated at') }}</th>
                                <th scope="col" class="text-center">{{ __('Actions') }}</th>
                                <th scope="col" class="text-center">{{ __('Control') }}</th>
                            </tr>
                            </thead>
                            <tbody class="list">
                            @foreach($plans as $plan)
                                <tr>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <div class="media-body">
                                                <span class="name mb-0 text-sm">{{ $plan->project_name }}</span>
                                            </div>
                                        </div>
                                    </th>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <div class="media-body">
                                                <span class="name mb-0 text-sm">{{ $plan->rooms_squares }}</span>
                                            </div>
                                        </div>
                                    </th>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <div class="media-body">
                                                <span class="name mb-0 text-sm">{{ $plan->bath_squares }}</span>
                                            </div>
                                        </div>
                                    </th>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <div class="media-body">
                                                <span class="name mb-0 text-sm">{{ $plan->room_count }}</span>
                                            </div>
                                        </div>
                                    </th>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <div class="media-body">
                                                <span class="name mb-0 text-sm">{{ $plan->sum }}€</span>
                                            </div>
                                        </div>
                                    </th>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <div class="media-body">
                                                <span class="name mb-0 text-sm">{{ $plan->sum_vat }}€</span>
                                            </div>
                                        </div>
                                    </th>
                                    <th scope="row">
                                        <div class="media align-items-center">
                                            <div class="media-body text-center">
                                                <span class="name mb-0 text-sm">{{ $plan->square_price }}€</span>
                                            </div>
                                        </div>
                                    </th>
                                    <td class="budget">{{ $plan->updated_at }}</td>
                                    <td class="text-center">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v mg-t-10"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a target="_blank" rel="noopener noreferrer" class="dropdown-item"
                                                   href="{{ route('plan.export', $plan) }}">
                                                    {{ __('Export') }}
                                                </a>
                                                <a class="dropdown-item"
                                                   href="{{ route('plan.replicate', $plan) }}">
                                                    {{ __('Clone') }}
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="dropdown">
                                            <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v mg-t-10"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item"
                                                   href="{{ route('plan.edit', $plan) }}">
                                                    {{ __('Edit') }}
                                                </a>
                                                <a data-toggle="modal" onclick="deleteData({{$plan->id}}, '{{$plan->project_name}}')"
                                                   data-target="#DeleteModal" class="dropdown-item">
                                                    {{ __('Delete') }}
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <!-- Card footer -->
                    <div class="card-footer py-4">
                        <nav aria-label="...">
                            <ul class="pagination justify-content-end mb-0">
                                {{ $plans->appends(['search' => Request::get('search')])->links('vendor.pagination.bootstrap-4') }}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('modal')
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/components/script.js') }}"></script>
    <script>
        function deleteData(id, name) {
            let url = '{{ route("plan.destroy", ":id") }}'
            url = url.replace(':id', id)
            document.getElementById('deleteInfo').innerText = '{{ __('Deleting plan -')}}' + ' ' + name
            $("#deleteForm").attr('action', url)
        }
        function formSubmit() {
            $("#deleteForm").submit();
        }
    </script>
@endpush
