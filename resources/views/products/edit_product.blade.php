@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
    <div class="header bg-gradient-primary py-4 py-lg-3">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header border-0">
                        <h3 class="mb-0">{{ __('Edit product') }}</h3>
                    </div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{ __(session('success')) }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('prd.update', $product->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <input type="hidden" name="price" id="price" value="{{ old('prime_cost') ? old('prime_cost') : $product->prime_cost }}">
                            <input type="hidden" name="price_vat" id="price_vat" value="{{ old('prime_cost') ? old('prime_cost') : $product->prime_cost }}">
                            <input type="hidden" name="code" id="code" value="">
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">
                                    {{ __('Name') }} <span class="red">*</span>
                                </label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name" value="{{ old('name') ? old('name') : $product->name }}" required autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prime_cost" class="col-md-4 col-form-label text-md-right">
                                    {{ __('Prime cost') }} <span class="red">*</span>
                                </label>

                                <div class="col-md-6">
                                    <input id="prime_cost" type="number" step="0.01"
                                           class="form-control @error('prime_cost') is-invalid @enderror"
                                           name="prime_cost" value="{{ old('prime_cost') ? old('prime_cost') : $product->prime_cost }}" required>

                                    @error('prime_cost')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="surcharge" class="col-md-4 col-form-label text-md-right">
                                    {{ __('Surcharge') }}(%) <span class="red">*</span>
                                </label>

                                <div class="col-md-6">
                                    <input id="surcharge" type="number" step="0.01"
                                           class="form-control @error('surcharge') is-invalid @enderror"
                                           name="surcharge" value="{{ old('surcharge') ? old('surcharge') : $product->surcharge }}" required>

                                    @error('surcharge')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="units_per_square" class="col-md-4 col-form-label text-md-right">
                                    {{ __('Units per work') }} <span class="red">*</span>
                                </label>

                                <div class="col-md-6">
                                    <input id="units_per_square" type="number" step="0.01"
                                           class="form-control @error('units_per_square') is-invalid @enderror" name="units_per_square"
                                           value="{{ old('units_per_square') ? old('units_per_square') : $product->units_per_square }}" required>

                                    @error('units_per_square')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="work_id" class="col-md-4 col-form-label text-md-right">
                                    {{ __('Work') }} <span class="red">*</span>
                                </label>

                                <div class="col-md-6">
                                    <select id="work_id" class="select-style form-control @error('work_id') is-invalid @enderror"
                                            name="work_id" required>
                                        @foreach($works as $work)
                                            @if (old('work_id') == $work->id || $product->work_id == $work->id)
                                                <option selected value="{{$work->id}}">{{$work->short_form}} - {{$work->name}}</option>
                                            @else
                                                <option value="{{$work->id}}">{{$work->short_form}} - {{$work->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>

                                    @error('$work_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                                <div class="col-md-6">
                                <textarea id="description" class="form-control select-color @error('description') is-invalid @enderror"
                                          name="description" rows="6">{{ old('description') ? old('description') : $product->description }}</textarea>
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Image') }}</label>

                                <div class="col-md-6">
                                    <div class="custom-file">
                                        <input type="file" name="image"
                                               class="custom-file-input form-control @error('image') is-invalid @enderror"
                                               value="{{ $product->image ? asset('images/' . $product->image->image_path) : "" }}">
                                        <label class="custom-file-label" for="image">{{ __('Select files') }}</label>

                                        @error('image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="editable" class="col-md-4 col-form-label text-md-right">
                                    {{ __('Editable') }} <span class="red">*</span>
                                </label>

                                <div class="col-md-6">
                                    <select id="editable" class="select-style form-control @error('editable') is-invalid @enderror"
                                            name="editable" required>
                                        <option {{ $product->editable || old('editable') ? 'selected' : '' }} value="1">{{ __('Editable') }}</option>
                                        <option {{ $product->editable || old('editable') ? '' : 'selected' }} value="0">{{ __('Uneditable') }}</option>
                                    </select>

                                    @error('editable')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="category_id" class="col-md-4 col-form-label text-md-right">
                                    {{ __('Active') }} <span class="red">*</span>
                                </label>

                                <div class="col-md-6">
                                    <select id="active" class="select-style form-control @error('active') is-invalid @enderror"
                                            name="active" required>
                                        <option {{ $product->active || old('active') ? 'selected' : '' }} value="1">{{ __('Active') }}</option>
                                        <option {{ $product->active || old('active') ? '' : 'selected' }} value="0">{{ __('Inactive') }}</option>
                                    </select>

                                    @error('active')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="divider-form"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('assets') }}/js/components/hidden.js"></script>
@endpush
