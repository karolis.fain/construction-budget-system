@extends('layouts.app', ['class' => 'bg-default'])

@section('content')
    <div class="header bg-gradient-primary py-4 py-lg-3">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header border-0">
                        <h3 class="mb-0">{{ __('Create new work') }}</h3>
                    </div>
                    <form method="POST" action="{{ route('work.store') }}" enctype="multipart/form-data">
                        @csrf

                        <input type="hidden" name="price" id="price" value="{{ old('price') }}">
                        <input type="hidden" name="price_vat" id="price_vat" value="{{ old('price_vat') }}">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">
                                {{ __('Name') }} <span class="red">*</span>
                            </label>

                            <div class="col-md-6">
                                <input id="name" type="text"
                                       class="form-control @error('name') is-invalid @enderror"
                                       name="name" value="{{ old('name') }}" required autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="short_form" class="col-md-4 col-form-label text-md-right">{{ __('Short form') }}</label>

                            <div class="col-md-6">
                                <input id="short_form" type="text"
                                       class="form-control @error('short_form') is-invalid @enderror"
                                       name="short_form" value="{{ old('short_form') }}" required>

                                @error('short_form')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="quantity" class="col-md-4 col-form-label text-md-right">
                                {{ __('Prime cost') }} <span class="red">*</span>
                            </label>

                            <div class="col-md-6">
                                <input id="prime_cost" type="number" step="0.01" class="form-control @error('prime_cost') is-invalid @enderror" name="prime_cost" value="{{ old('prime_cost') }}" required>

                                @error('prime_cost')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="surcharge" class="col-md-4 col-form-label text-md-right">
                                {{ __('Surcharge') }}(%) <span class="red">*</span>
                            </label>

                            <div class="col-md-6">
                                <input id="surcharge" type="number" step="0.01"
                                       class="form-control @error('surcharge') is-invalid @enderror"
                                       name="surcharge" value="{{ old('surcharge') }}" required>

                                @error('surcharge')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category_id" class="col-md-4 col-form-label text-md-right">
                                {{ __('Category') }} <span class="red">*</span>
                            </label>

                            <div class="col-md-6">
                                <select id="category_id" class="select-style form-control @error('category_id') is-invalid @enderror"
                                        name="category_id" required>
                                    @foreach($categories as $category)
                                        @if (old('category_id') == $category->id)
                                            <option selected value="{{$category->id}}">{{$category->short_form}} - {{$category->name}}</option>
                                        @else
                                            <option value="{{$category->id}}">{{$category->short_form}} - {{$category->name}}</option>
                                        @endif
                                    @endforeach
                                </select>

                                @error('category_id')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="unit_id" class="col-md-4 col-form-label text-md-right">
                                {{ __('Unit') }} <span class="red">*</span>
                            </label>

                            <div class="col-md-6">
                                <select id="unit_id" class="select-style form-control @error('unit_id') is-invalid @enderror"
                                        name="unit_id" required>
                                    @foreach($units as $unit)
                                        @if (old('unit_id') == $unit->id)
                                            <option selected value="{{$unit->id}}">{{$unit->name}}</option>
                                        @else
                                            <option value="{{$unit->id}}">{{$unit->name}}</option>
                                        @endif
                                    @endforeach
                                </select>

                                @error('units')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="category_id" class="col-md-4 col-form-label text-md-right">
                                {{ __('Active') }} <span class="red">*</span>
                            </label>

                            <div class="col-md-6">
                                <select id="active" class="select-style form-control @error('active') is-invalid @enderror"
                                        name="active" required>
                                    <option {{ old('active') ? 'selected' : '' }} value="1">{{ __('Active') }}</option>
                                    <option {{ old('active') ? '' : 'selected' }} value="0">{{ __('Inactive') }}</option>
                                </select>

                                @error('active')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="divider-form"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('assets') }}/js/components/work.js"></script>
@endpush
