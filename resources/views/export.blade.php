<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        * {
            font-family:"DeJaVu Sans Mono",monospace;
            font-size: 10px;
        }
        .bg-default {
            background: linear-gradient(87deg, #5e72e4 0, #825ee4 100%);
        }
        .main-content {
            position: relative;
        }
        .header {
            position: relative;
        }
        .bg-gradient-primary
        {
            background: linear-gradient(87deg, #5e72e4 0, #825ee4 100%);
        }
        .table {
            border-collapse:collapse;
        }
        .plan-table {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        .plan-table td, .plan-table th {
            border: 1px solid #1f2024;
            padding: 0 3px 0 3px;
        }
        .plan-table th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #114d65;
            color: white;
        }
        .info-table td, info-table tr {
            padding: 0;
            margin: 0;
        }
        .category-color {
            color: white;
            background-color: #15607e;
            font-size: 12px;
        }
        .page-break {
            page-break-after: always;
        }
        .row-work td {
            background-color: #e6e4e5;
            font-size: 9px;
        }
        .row-data td{
            font-size: 9px;
        }
        h2 {
            font-size: 20px;
            background-color: #e4e4e4;
        }
        th {
            font-size: 11px;
        }
        p, b {
            font-size: 12px;
        }
        .column {
            float: left;
            padding: 10px;
        }
        .left {
            width: 30%;
        }
        .right {
            width: 30%;
        }
        .middle {
            width: 30%;
        }
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
        .divider-form-lg {
            overflow: hidden;
            height: 0;
            margin: 2rem 0;
        }
        .divider-form-sm {
            overflow: hidden;
            height: 0;
            margin: .5rem 0;
        }
        .image-border {
            background-color: #e4e4e4;
        }
        .text-p {
            font-size: 10px;
            height: 60px;
            overflow: hidden;
            padding-bottom: 0;
            padding-left: 0;
            padding-right: 0;
            margin-bottom: 0;
            margin-left: 0;
            margin-right: 0;
        }
    </style>
</head>
<body class="bg-default">

<div class="main-content">
    <div class="header bg-gradient-primary">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header border-0">
                        <h2 class="mb-0">{{ __('Plan structure') }}
                            <img width="25%" style="margin-left: 15%" src="{{ asset('/assets/img/brand/anvi.png') }}"/>
                        </h2>
                    </div>
                    <table>
                        <tr>
                            <td><b>Anvi projektai, UAB</b></td>
                        </tr>
                        <tr>
                            <td><b>Įm.k.: </b>303778899, <b>PVM mok.k. </b>LT100009199018</td>
                        </tr>
                        <tr>
                            <td><b>Tel. numeris: </b>+370 673 72 168, <b>El. paštas: </b>info@anviprojektai.lt</td>
                        </tr>
                        <tr>
                            <td><b>Adresas: </b>Piliakalnio g. 43, LT-46227 Kaunas, Lietuva</td>
                        </tr>
                    </table>
                    <div class="divider-form-lg"></div>
                    <p><b>Projektas: </b>{{ $plan->project_name }}</p>
                    <p><b>{{ __('Data:') }}</b> {{ date('Y-m-d H:i:s') }}</p>
                    <div class="row">
                        <div class="column left" style="background-color:#ccc;">
                            <table>
                                <tr>
                                    <td><b>{{ __('Rooms count') }}:</b></td>
                                    <td>{{ $plan->room_count }}</td>
                                </tr>
                                <tr>
                                    <td><b>{{ __('Rooms squares') }}:</b></td>
                                    <td>{{ $plan->rooms_squares }}m<sup>2</sup></td>
                                </tr>
                                <tr>
                                    <td><b>{{ __('Bath/WC squares') }}:</b></td>
                                    <td>{{ $plan->bath_squares }}m<sup>2</sup></td>
                                </tr>
                            </table>
                        </div>
                        <div class="column middle"></div>
                        <div class="column right" style="background-color:#ccc;">
                            <table>
                                <tr>
                                    <td><b>{{ __('Total sum') }}:</b></td>
                                    <td>{{ $plan->sum }}&euro;</td>
                                </tr>
                                <tr>
                                    <td><b>{{ __('Total sum (VAT)') }}:</b> </td>
                                    <td>{{ $plan->sum_vat }}&euro;</td>
                                </tr>
                                <tr>
                                    <td><b>1m<sup>2</sup> {{ __('price (VAT)') }}:</b></td>
                                    <td>{{ $plan->square_price }}&euro;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="divider-form-sm"></div>
                    <div class="plan-table">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th>{{ __('Nr.') }}</th>
                                <th>{{ __('Work/material name') }}</th>
                                <th>{{ __('Unit me.') }}</th>
                                <th>{{ __('Quantity') }}</th>
                                <th>{{ __('Work price') }}</th>
                                <th>{{ __('Un. price') }}</th>
                                <th>{{ __('Sum price') }}</th>
                                <th>{{ __('Sum full price') }}</th>
                            </tr>
                            </thead>
                            <tbody class="list">
                            <tbody id="plan-table-content">
                            @php($count=0)
                            @foreach($catsWorksProducts as $category => $categoryWorks)
                                <tr class="category-row">
                                    <td colspan="8" class="category-color">{{ $category }}</td>
                                </tr>
                                @php($sum=0)
                                @foreach($categoryWorks as $work => $workProducts)
                                    @php($count++)
                                    <tr class="row-work">
                                        <td style="width: 5%">{{ $count }}</td>
                                        <td style="width: 35%">
                                            <div class="media align-items-center">
                                                <div class="media-body">
                                                    <span class="name mb-0 text-sm">{{ $works[$work]->name }}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width: 5%">
                                            <div class="media align-items-center">
                                                <div class="media-body">
                                                    <span class="name mb-0 text-sm">{{ $works[$work]->unit->short_form }}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="media align-items-center">
                                                <div class="media-body">
                                                    <span class="name mb-0 text-sm">{{ $works[$work]->pivot->quantity }}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="media align-items-center">
                                                <div class="media-body">
                                                    <span class="name mb-0 text-sm work-price">{{ $works[$work]->price }}&euro;</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td scope="row">
                                            <div class="media align-items-center">
                                                <div class="media-body">
                                                    <span class="name mb-0 text-sm value">{{ number_format($works[$work]->full_price, 2) }}&euro;</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td scope="row">
                                            <div class="media align-items-center">
                                                <div class="media-body">
                                                    <span class="name mb-0 text-sm sum">{{ $workSums[$work] }}&euro;</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @foreach($workProducts as $product)
                                        <tr class="row-data">
                                            <td></td>
                                            <td>{{ $product->name }}</td>
                                            <td></td>
                                            <td>{{ $product->pivot->quantity == 0 ? number_format($works[$work]->pivot->quantity * $product->units_per_square, 2) : $product->pivot->quantity }}</td>
                                            <td></td>
                                            <td>
                                                <div class="media align-items-center">
                                                    <div class="media-body">
                                                        <span class="name mb-0 text-sm">{{ $product->price }}&euro;</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="media align-items-center">
                                                    <div class="media-body">
                                                        {{ $product->full_price }}&euro;
                                                    </div>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endforeach
                            </tbody>
                        </table>
                        @if(!$images->isEmpty())
                            <div class="page-break"></div>
                            <h2>
                                {{ __('Substances') }}
                                <img width="25%" style="margin-left: 56%" src="{{ asset('/assets/img/brand/anvi.png') }}"/>
                            </h2>
                            <table>
                                @foreach($images->chunk(3) as $imagesChunk)
                                    <tr>
                                        @foreach($imagesChunk as $image)
                                            <td style="padding: 8px" class="image-border">
                                                <img width="215px" src="{{ asset($image['image_path']) }}"/>
                                                <p class="text-p">{{ $image['description'] }}</p>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
