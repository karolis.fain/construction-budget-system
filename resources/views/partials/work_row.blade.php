<tr class="row-work">
    <th></th>
    <input type="hidden" name="works[]" value="{{ $work->id }}">
    <input type="hidden" name="units" value="{{ $work->units_per_square }}">
    <input type="hidden" name="type" value="{{ $work->type }}">
    <th>
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm">{{ $work->name }}</span>
            </div>
        </div>
    </th>
    <th scope="row">
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm">{{ $work->unit->short_form }}</span>
            </div>
        </div>
    </th>
    <th>
        <input value="{{ $quantity }}" type="number" min="0" step="0.01" class="unit-style quantity" name="quantity[]" required>
    </th>
    <th>
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm work-price">{{ $work->price }}€</span>
            </div>
        </div>
    </th>
    <th>
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm"></span>
            </div>
        </div>
    </th>
    <th scope="row">
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm value">0</span>
            </div>
        </div>
    </th>
    <th scope="row">
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm sum">0</span>
            </div>
        </div>
    </th>
    <th scope="row">
        <div class="media align-items-center">
            <div class="media-body delete-work">
                <i class="fas fa-ban"></i> {{ __('Delete') }}
            </div>
        </div>
    </th>
</tr>
