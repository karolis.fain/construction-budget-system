<tr class="category-row">
    <th colspan="8" class="category-color">{{ $category->name }}</th>
    <th colspan="1" class="category-color">
        <div class="media align-items-center">
            <div class="media-body delete-category" id="{{ $category->id }}">
                <i class="fas fa-ban"></i> {{ __('Delete') }}
            </div>
        </div>
    </th>
</tr>
<tr class="work-select">
    <th colspan="1"></th>
    <th colspan="2">
        <div class="center-align mg-r-15">
            <i class="fas fa-plus"></i>{{ __('Add new work') }}
        </div>
    </th>
    <th colspan="6">
        <select class="select-style form-control work">
            <option hidden selected disabled value>{{ __('Select work') }}</option>
            @foreach ($works as $work)
                <option value="{{ $work->id }}">
                    {{ $work->name }} - {{ $work->price }}€
                </option>
            @endforeach
        </select>
    </th>
</tr>
