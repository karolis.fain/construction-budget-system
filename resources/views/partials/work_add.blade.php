<tr class="row-work">
    <th></th>
    <input type="hidden" name="works[]" value="{{ $work->id }}">
    <input type="hidden" name="units" value="{{ $work->units_per_square }}">
    <input type="hidden" name="type" value="{{ $work->type }}">
    <th>
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm">{{ $work->name }}</span>
            </div>
        </div>
    </th>
    <th scope="row">
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm">{{ $work->unit->short_form }}</span>
            </div>
        </div>
    </th>
    <th>
        <input type="number" step="0.01" min="0" class="unit-style quantity" name="quantity[]" required>
    </th>
    <th>
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm work-price">{{ $work->price }}€</span>
            </div>
        </div>
    </th>
    <th></th>
    <th scope="row">
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm value">0</span>
            </div>
        </div>
    </th>
    <th scope="row">
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm sum">0</span>
            </div>
        </div>
    </th>
    <th scope="row">
        <div class="media align-items-center">
            <div class="media-body delete-work">
                <i class="fas fa-ban"></i> {{ __('Delete') }}
            </div>
        </div>
    </th>
</tr>
@if ($products->isNotEmpty())
    <tr class="product-select">
        <th colspan="2"></th>
        <th colspan="2">
            <div class="center-align mg-r-15">
                <i class="fas fa-plus"></i>{{ __('Add product') }}
            </div>
        </th>
        <th colspan="4">
            <select class="select-style form-control product">
                <option hidden selected disabled value>{{ __('Select product') }}</option>
                @foreach ($products as $product)
                    <option value="{{ $product->id }}">
                        {{ $product->name }} - {{ $product->price }}€
                    </option>
                @endforeach
            </select>
        </th>
    </tr>
@endif
