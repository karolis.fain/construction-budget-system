<tr class="category-row">
    <th colspan="8" class="category-color">{{ $category }}</th>
    <th colspan="1" class="category-color">
        <div class="media align-items-center">
            <div class="media-body delete-category" id="{{ $categories->firstWhere('name', $category)->id }}">
                <i class="fas fa-ban"></i> {{ __('Delete') }}
            </div>
        </div>
    </th>
</tr>
