<tr class="product-select" @if ($disabled) style="display: none" @endif>
    <th colspan="2"></th>
    <th colspan="2">
        <div class="center-align mg-r-15">
            <i class="fas fa-plus"></i>{{ __('Add product') }}
        </div>
    </th>
    <th colspan="5">
        <select class="select-style form-control product">
            <option hidden disabled selected value>{{ __('Select product') }}</option>
            @foreach ($products as $product)
                <option value="{{ $product->id }}"
                        @foreach($usedProducts as $used)
                            @if ($product->id == $used->id)
                                disabled
                                @break
                            @endif
                        @endforeach
                >
                    {{ $product->name }} - {{ $product->price }}€
                </option>
            @endforeach
        </select>
    </th>
</tr>
