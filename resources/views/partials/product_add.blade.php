<tr class="row-product">
    <th></th>
    <input type="hidden" name="products[]" value="{{ $product->id }}">
    <input type="hidden" name="units" value="{{ ($product->pivot->quantity ?? 0) == 0 ? $product->units_per_square : $product->pivot->quantity }}">
    <th>
        <div class="media align-items-center">
            <div class="media-body">
                @if ($product->image)
                    <i class="fas fa-info-circle relative-pos image-pop">
                        <img class="image-show" alt="product" style="display: none" src="{{ asset('images/' . $product->image->image_path) }}">
                    </i>
                @endif
                <span class="name mb-0 text-sm">{{ $product->name }}</span>
            </div>
        </div>
    </th>
    <th></th>
    <th scope="row">
        @if ($product->editable)
            <input type="number" step="0.01" min="0" class="unit-style quantity units-per"
                   value="{{ $product->pivot->quantity ?? $product->units_per_square }}" name="prod_quantity[{{$product->id}}]" required>
        @else
            <div class="media align-items-center">
                <div class="media-body">
                    <span class="name mb-0 text-sm units-per">{{ $product->units_per_square }}</span>
                </div>
            </div>
        @endif
    </th>
    <th></th>
    <th>
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm unit-price">{{ $product->price }}€</span>
            </div>
        </div>
    </th>
    <th scope="row">
        <div class="media align-items-center">
            <div class="media-body">
                <span class="name mb-0 text-sm value">0</span>
            </div>
        </div>
    </th>
    <th></th>
    <th scope="row">
        <div class="media align-items-center">
            <div class="media-body delete-product">
                <i class="fas fa-ban"></i> {{ __('Delete') }}
            </div>
        </div>
    </th>
</tr>
