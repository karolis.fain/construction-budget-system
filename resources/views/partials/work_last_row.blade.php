<tr class="work-select" @if ($disabled) style="display: none" @endif>
    <th colspan="1"></th>
    <th colspan="2">
        <div class="center-align mg-r-15">
            <i class="fas fa-plus"></i>{{ __('Add new work') }}
        </div>
    </th>
    <th colspan="6">
        <select class="select-style form-control work">
            <option hidden disabled selected value>{{ __('Select work') }}</option>
            @foreach ($works as $work)
                <option value="{{ $work->id }}"
                        @foreach($usedWorks as $index => $used)
                            @if ($work->name == $index)
                                disabled
                                @break
                            @endif
                        @endforeach
                >
                    {{ $work->name }} - {{ $work->price }}€
                </option>
            @endforeach
        </select>
    </th>
</tr>
