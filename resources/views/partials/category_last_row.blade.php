<tr class="category-row" @if ($disabled) style="display: none" @endif>
    <th colspan="2">
        <div class="center-align mg-r-15">
            <i class="fas fa-plus"></i>{{ __('Add new category') }}
        </div>
    </th>
    <th colspan="7">
        <select id="add-category" class="select-style form-control" name="category_id">
            <option hidden selected disabled value>{{ __('Select category') }}</option>
            @foreach($categories as $category)
                <option
                    @foreach($usedCategories as $index => $used)
                        @if ($category->name == $index)
                            disabled
                            @break
                        @endif
                    @endforeach
                    value="{{ $category->id }}"
                >
                    {{$category->name}}
                </option>
            @endforeach
        </select>
    </th>
</tr>
