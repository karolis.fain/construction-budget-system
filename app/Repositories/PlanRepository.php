<?php

namespace App\Repositories;

use App\Models\Plan;
use FontLib\Table\Type\name;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

class PlanRepository
{
    private array $searchFields = [ 'project_name', 'rooms_squares',
        'bath_squares', 'room_count', 'sum', 'sum_vat', 'square_price',
        'updated_at' ];

    /**
     * Create new plan
     *
     * @param array $attributes
     * @return Plan
     */
    public function create(array $attributes): Plan
    {
        $plan = Plan::create($attributes);

        if (isset($attributes['products'])) {
            foreach ($attributes['products'] as $product) {
                $quantity = $attributes['prod_quantity'][$product] ?? 0;
                $plan->products()->attach($product, ['quantity' => $quantity]);
            }
        }

        foreach ($attributes['works'] as $index => $work) {
            $plan->works()->attach($work, ['quantity' => $attributes['quantity'][$index]]);
        }

        return $plan;
    }

    /**
     * Create new plan
     *
     * @param array $attributes
     * @param int $id
     * @return bool
     */
    public function update(array $attributes, int $id): bool
    {
        $plan = Plan::findOrFail($id);

        $result = $plan->update($attributes);

        $plan->products()->detach();
        if (isset($attributes['products'])) {
            foreach ($attributes['products'] as $product) {
                $quantity = $attributes['prod_quantity'][$product] ?? 0;
                $plan->products()->attach($product, ['quantity' => $quantity]);
            }
        }

        $plan->works()->detach();
        foreach ($attributes['works'] as $index => $work) {
            $plan->works()->attach($work, ['quantity' => $attributes['quantity'][$index]]);
        }

        return $result;
    }

    /**
     * @return Plan|null
     */
    public function getBasePlan(): ?Plan
    {
        return Plan::findOrFail(1);
    }

    /**
     * Get all plans
     *
     * @return LengthAwarePaginator
     */
    public function getAll(): ?LengthAwarePaginator
    {
        return Plan::where('id', '!=', 1)->latest()->paginate(20);
    }

    /**
     * Search all plans
     *
     * @param string $search
     * @return LengthAwarePaginator
     */
    public function searchAll(string $search): ?LengthAwarePaginator
    {
        return $this->searchQuery($search)->latest()->paginate(20);
    }

    /**
     * Search by specified search term
     *
     * @param string $searchTerm
     * @return Builder
     */
    public function searchQuery(string $searchTerm): Builder
    {
        return Plan::query()
            ->whereLike($this->searchFields, $searchTerm)
            ->where('id', '!=', 1)
            ->latest();
    }

    /**
     * Get plan by ID
     *
     * @param int $id
     * @return Plan|null
     */
    public function getById(int $id): ?Plan
    {
        return Plan::findOrFail($id);
    }

    /**
     * Delete plan by ID
     *
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool
    {
        $plan = Plan::findOrFail($id);
        $plan->products()->sync([]);
        $plan->works()->sync([]);

        return Plan::destroy($id);
    }
}
