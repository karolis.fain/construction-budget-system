<?php

namespace App\Repositories;

use App\Models\Image;

class ImageRepository
{
    /**
     * Create image record
     *
     * @param array $image
     * @return Image
     */
    public function saveImage(array $image)
    {
        return Image::create($image);
    }

    /**
     * Find image by id
     *
     * @param int $id
     * @return Image
     */
    public function findById(int $id): Image
    {
        return Image::findOrFail($id);
    }

    /**
     * Delete image
     *
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return Image::destroy($id);
    }
}
