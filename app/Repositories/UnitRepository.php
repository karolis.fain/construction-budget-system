<?php

namespace App\Repositories;

use App\Models\Unit;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class UnitRepository
{
    private array $searchFields = ['name', 'short_form', 'created_at', 'updated_at'];

    /**
     * Create new unit
     *
     * @param array $attributes
     * @return Unit
     */
    public function create(array $attributes): Unit
    {
        return Unit::create($attributes);
    }

    /**
     * Create new unit
     *
     * @param array $attributes
     * @param int $id
     * @return bool
     */
    public function update(array $attributes, int $id): bool
    {
        $unit = Unit::findOrFail($id);

        return $unit->update($attributes);
    }

    /**
     * Get all categories
     *
     * @return LengthAwarePaginator
     */
    public function getAll(): ?LengthAwarePaginator
    {
        return Unit::latest()->paginate(20);
    }

    /**
     * Search all categories
     *
     * @param string $search
     * @return LengthAwarePaginator
     */
    public function searchAll(string $search): ?LengthAwarePaginator
    {
        return $this->searchQuery($search)->paginate(20);
    }

    /**
     * @return Collection
     */
    public function getUnitsCollection(): Collection
    {
        return Unit::where('active', true)->get();
    }

    /**
     * Search by specified search term
     *
     * @param string $searchTerm
     * @return Builder
     */
    public function searchQuery(string $searchTerm): Builder
    {
        return Unit::query()
            ->whereLike($this->searchFields, $searchTerm)
            ->latest();
    }

    /**
     * Get unit by ID
     *
     * @param int $id
     * @return Unit|null
     */
    public function getById(int $id): ?Unit
    {
        return Unit::findOrFail($id);
    }

    /**
     * Delete unit by ID
     *
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool
    {
        return Unit::destroy($id);
    }
}
