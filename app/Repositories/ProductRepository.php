<?php

namespace App\Repositories;

use App\Models\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class ProductRepository
{
    private array $searchFields = [ 'work.name', 'name', 'code',
        'price', 'prime_cost', 'surcharge', 'units_per_square',
        'description', 'updated_at' ];

    /**
     * Create new product
     *
     * @param array $attributes
     * @return Product
     */
    public function create(array $attributes): Product
    {
        return Product::create($attributes);
    }

    /**
     * Create new product
     *
     * @param array $attributes
     * @param int $id
     * @return bool
     */
    public function update(array $attributes, int $id): bool
    {
        $product = Product::findOrFail($id);

        return $product->update($attributes);
    }

    /**
     * Get all products
     *
     * @return LengthAwarePaginator
     */
    public function getAll(): ?LengthAwarePaginator
    {
        return Product::with('image')
            ->latest('updated_at')
            ->paginate(15);
    }

    /**
     * Search all products
     *
     * @param string $search
     * @return LengthAwarePaginator
     */
    public function searchAll(string $search): ?LengthAwarePaginator
    {
        return $this->searchQuery($search)
            ->paginate(15);
    }

    /**
     * Search by specified search term
     *
     * @param string $searchTerm
     * @return Builder
     */
    public function searchQuery(string $searchTerm): Builder
    {
        return Product::query()
            ->whereLike($this->searchFields, $searchTerm)
            ->latest('updated_at');
    }

    /**
     * @return Collection
     */
    public function getProductsCollection(): Collection
    {
        return Product::where('active', true)->with('work')->get();
    }

    /**
     * Get product by ID
     *
     * @param int $id
     * @return Product|null
     */
    public function getById(int $id): ?Product
    {
        return Product::findOrFail($id);
    }

    /**
     * @param int $id
     * @return Collection
     */
    public function getByWorkId(int $id): Collection
    {
        return Product::where('work_id', $id)
            ->where('active', true)->get();
    }

    /**
     * Delete product by ID
     *
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool
    {
        return Product::destroy($id);
    }
}
