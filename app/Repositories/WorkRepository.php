<?php

namespace App\Repositories;

use App\Models\Work;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class WorkRepository
{
    private array $searchFields = [ 'name', 'category.name', 'unit.name',
        'short_form', 'price', 'updated_at' ];

    /**
     * Create new work
     *
     * @param array $attributes
     * @return Work
     */
    public function create(array $attributes): Work
    {
        return Work::create($attributes);
    }

    /**
     * Create new work
     *
     * @param array $attributes
     * @param int $id
     * @return bool
     */
    public function update(array $attributes, int $id): bool
    {
        $work = Work::findOrFail($id);

        return $work->update($attributes);
    }

    /**
     * Get all works
     *
     * @return LengthAwarePaginator
     */
    public function getAll(): ?LengthAwarePaginator
    {
        return Work::latest('updated_at')
            ->paginate(20);
    }

    /**
     * Search all works
     *
     * @param string $search
     * @return LengthAwarePaginator
     */
    public function searchAll(string $search): ?LengthAwarePaginator
    {
        return $this->searchQuery($search)
            ->paginate(20);
    }

    /**
     * Search by specified search term
     *
     * @param string $searchTerm
     * @return Builder
     */
    public function searchQuery(string $searchTerm): Builder
    {
        return Work::query()
            ->whereLike($this->searchFields, $searchTerm)
            ->latest('updated_at');
    }

    /**
     * Get all works in collection
     *
     * @return Collection
     */
    public function getWorksCollection(): Collection
    {
        return Work::where('active', true)->with('category')->get();
    }

    /**
     * Get work by ID
     *
     * @param int $id
     * @return Work|null
     */
    public function getById(int $id): ?Work
    {
        return Work::findOrFail($id);
    }

    /**
     * @param int $id
     * @return Collection
     */
    public function getByCatId(int $id): Collection
    {
        return Work::where('category_id', $id)
            ->where('active', true)->get();
    }

    /**
     * @param string $field
     * @param int $value
     * @return Collection
     */
    public function getByCustomField(string $field, int $value): Collection
    {
        return Work::where($field, $value)->get();
    }


    /**
     * Delete work by ID
     *
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool
    {
        return Work::destroy($id);
    }
}
