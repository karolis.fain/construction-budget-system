<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository
{
    private array $searchFields = ['name', 'short_form', 'created_at', 'updated_at'];

    /**
     * Create new category
     *
     * @param array $attributes
     * @return Category
     */
    public function create(array $attributes): Category
    {
        return Category::create($attributes);
    }

    /**
     * Create new category
     *
     * @param array $attributes
     * @param int $id
     * @return bool
     */
    public function update(array $attributes, int $id): bool
    {
        $category = Category::findOrFail($id);

        return $category->update($attributes);
    }

    /**
     * Get all categories
     *
     * @return LengthAwarePaginator
     */
    public function getAll(): ?LengthAwarePaginator
    {
        return Category::latest()->paginate(20);
    }

    /**
     * Search all categories
     *
     * @param string $search
     * @return LengthAwarePaginator
     */
    public function searchAll(string $search): ?LengthAwarePaginator
    {
        return $this->searchQuery($search)->paginate(20);
    }

    /**
     * Search by specified search term
     *
     * @param string $searchTerm
     * @return Builder
     */
    public function searchQuery(string $searchTerm): Builder
    {
        return Category::query()
            ->whereLike($this->searchFields, $searchTerm)
            ->latest();
    }

    /**
     * @return Collection
     */
    public function getCategoriesCollection(): Collection
    {
        return Category::where('active', true)->get();
    }

    /**
     * Get category by ID
     *
     * @param int $id
     * @return Category|null
     */
    public function getById(int $id): ?Category
    {
        return Category::findOrFail($id);
    }

    /**
     * Delete category by ID
     *
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool
    {
        return Category::destroy($id);
    }
}
