<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_name',
        'rooms_squares',
        'bath_squares',
        'room_count',
        'sum',
        'sum_vat',
        'square_price'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class)
            ->withPivot('quantity');
    }

    public function works()
    {
        return $this->belongsToMany(Work::class)
            ->withPivot('quantity');
    }
}
