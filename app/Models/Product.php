<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $attributes = array(
        'price' => 0,
        'price_vat' => 0,
        'code' => ''
    );

    protected $fillable = [
        'code',
        'work_id',
        'image_id',
        'name',
        'description',
        'prime_cost',
        'price',
        'price_vat',
        'surcharge',
        'units_per_square',
        'editable',
        'active'
    ];

    public function work()
    {
        return $this->belongsTo(Work::class);
    }

    public function image()
    {
        return $this->hasOne(Image::class);
    }

    public function plans()
    {
        return $this->belongsToMany(Plan::class)
            ->withPivot('quantity');
    }

    public function setCodeAttribute($val) {
        if ($this->getOriginal('code')) {
            $num = filter_var($this->getOriginal('code'),FILTER_SANITIZE_NUMBER_INT);
            $this->attributes['code'] = strtoupper($val) . $num;
        } else {
            $number = Product::all()->last()->id ?? 0;
            $this->attributes['code'] = strtoupper($val) . (1500 + $number);
        }
    }

    public function getPrimeCostAttribute() {
        return number_format($this->attributes['prime_cost'] / 100, 2);
    }

    public function setPrimeCostAttribute($val) {
        $this->attributes['prime_cost'] = $val * 100;
    }


    public function getPriceAttribute() {
        return number_format($this->attributes['price'], 2);
    }

    /**
     * Get real price without 21%
     * @param $val
     */
    public function setPriceAttribute($val) {
        $this->attributes['price'] = $val + $this->attributes['surcharge'] / 100 * $val;
    }

    /**
     * Get real price with 21% and surcharge
     * @param $val
     */
    public function setPriceVatAttribute($val) {
        $this->attributes['price_vat'] = ($val + $this->attributes['surcharge'] / 100 * $val) * 1.21;
    }

    /**
     * Get full price attribute
     * @param string $val
     * @return void
     */
    public function setFullPriceAttribute($val)
    {
        if ($this->editable) {
            $this->attributes['full_price'] = number_format($this->price * $this->pivot->quantity, 2);
        } else {
            $this->attributes['full_price'] = number_format($this->price * $this->units_per_square * $val, 2);
        }
    }

}
