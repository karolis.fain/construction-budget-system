<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    use HasFactory;

    protected $attributes = array(
        'price' => 0,
        'price_vat' => 0,
    );

    protected $fillable = [
        'name',
        'category_id',
        'unit_id',
        'short_form',
        'prime_cost',
        'price',
        'price_vat',
        'surcharge',
        'active'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function plans()
    {
        return $this->belongsToMany(Plan::class)
            ->withPivot('quantity');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function getPrimeCostAttribute() {
        return number_format($this->attributes['prime_cost'] / 100, 2);
    }

    public function setPrimeCostAttribute($val) {
        $this->attributes['prime_cost'] = $val * 100;
    }


    public function getPriceAttribute() {
        return number_format($this->attributes['price'], 2);
    }

    /**
     * Get real price without 21%
     * @param $val
     */
    public function setPriceAttribute($val) {
        $this->attributes['price'] = $val + $this->attributes['surcharge'] / 100 * $val;
    }

    /**
     * Get real price with 21% and surcharge
     * @param $val
     */
    public function setPriceVatAttribute($val) {
        $this->attributes['price_vat'] = ($val + $this->attributes['surcharge'] / 100 * $val) * 1.21;
    }

    /**
     * Get full price attribute
     * @return float|int
     */
    public function getFullPriceAttribute()
    {
        return $this->price * $this->pivot->quantity;
    }
}
