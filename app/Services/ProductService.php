<?php

namespace App\Services;

use App\Models\Product;
use App\Repositories\ImageRepository;
use App\Repositories\ProductRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;

class ProductService
{
    protected const PRODUCT_IMAGES_DIR = 'images/product_images';

    /**
     * @var ProductRepository
     */
    public ProductRepository $productRepository;

    /**
     * @var ImageRepository
     */
    public ImageRepository $imageRepository;

    /**
     * ProductService constructor
     *
     * @param ProductRepository $productRepository
     * @param ImageRepository $imageRepository
     */
    public function __construct(ProductRepository $productRepository, ImageRepository $imageRepository)
    {
        $this->productRepository = $productRepository;
        $this->imageRepository = $imageRepository;
    }

    /**
     * Create new product
     *
     * @param array $attributes
     * @return Product
     */
    public function createProduct(array $attributes): Product
    {
        $product = $this->productRepository->create($attributes);

        if (isset($attributes['image'])) {
            $image = $this->saveFile($attributes['image'], $product->id);
            $this->imageRepository->saveImage($image);
        }

        return $product;
    }

    /**
     * Update product
     *
     * @param array $attributes
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function updateProduct(array $attributes, int $id): bool
    {
        if (isset($attributes['image'])) {
            $product = $this->productRepository->getById($id);
            if ($image = $product->image()->first()) {
                unlink(public_path() . "/images/" . $image->image_path);
                $image->delete();
            }
            $image = $this->saveFile($attributes['image'], $id);
            $this->imageRepository->saveImage($image);
        }

        return $this->productRepository->update($attributes, $id);
    }

    /**
     * Get all products
     *
     * @param string|null $search
     * @return LengthAwarePaginator
     */
    public function getProducts(?string $search = null): LengthAwarePaginator
    {
        if ($search) {
            return $this->productRepository->searchAll($search);
        }

        return $this->productRepository->getAll();
    }

    /**
     * @return Collection
     */
    public function getProductsCollection(): Collection
    {
        return $this->productRepository->getProductsCollection();
    }

    /**
     * Get product by ID
     *
     * @param int $id
     * @return Product
     */
    public function getProductById(int $id): Product
    {
        return $this->productRepository->getById($id);
    }

    /**
     * @param int $workId
     * @return Collection
     */
    public function getProductsByWorkId(int $workId): Collection
    {
        return $this->productRepository->getByWorkId($workId);
    }

    /**
     * Delete product by ID
     *
     * @param int $id
     * @return bool
     */
    public function deleteProductById(int $id): bool
    {
        return $this->productRepository->deleteById($id);
    }

    /**
     * Save product image
     *
     * @param UploadedFile $file
     * @param int $id
     * @return array
     */
    public function saveFile(UploadedFile $file, int $id)
    {
        $name = time() . '_' . $file->getClientOriginalName();
        $file->move(public_path('images'), $name);

        return [
            'product_id' => $id,
            'image_path' => $name
        ];
    }
}
