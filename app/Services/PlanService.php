<?php

namespace App\Services;

use App\Models\Plan;
use App\Models\Product;
use App\Models\Work;
use App\Repositories\PlanRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use \Illuminate\Support\Collection as ObjectCollection;
use phpDocumentor\Reflection\DocBlock\Tags\Formatter\AlignFormatter;

class PlanService
{
    /**
     * @var PlanRepository
     */
    public PlanRepository $planRepository;


    /**
     * PlanService constructor
     *
     * @param PlanRepository $planRepository
     */
    public function __construct(PlanRepository $planRepository)
    {
        $this->planRepository = $planRepository;
    }

    /**
     * Create new plan
     *
     * @param array $attributes
     * @return Plan
     */
    public function createPlan(array $attributes): Plan
    {
        return $this->planRepository->create($attributes);
    }

    /**
     * Update plan
     *
     * @param array $attributes
     * @param int|null $id
     * @return bool
     */
    public function updatePlan(array $attributes, ?int $id = null): bool
    {
        return $this->planRepository->update($attributes, $id);
    }

    /**
     * Get all plans
     *
     * @param string|null $search
     * @return LengthAwarePaginator
     */
    public function getPlans(?string $search = null): LengthAwarePaginator
    {
        if ($search) {
            return $this->planRepository->searchAll($search);
        }

        return $this->planRepository->getAll();
    }

    /**
     * @return Plan|null
     */
    public function getBasePlan(): ?Plan
    {
        return $this->planRepository->getBasePlan();
    }

    /**
     * Get plan by ID
     *
     * @param int $id
     * @return Plan
     */
    public function getPlanById(int $id): Plan
    {
        return $this->planRepository->getById($id);
    }

    /**
     * Group products by categories and works
     *
     * @param ObjectCollection $products
     * @param ObjectCollection $categories
     * @param ObjectCollection $works
     * @param ObjectCollection $worksQuantity
     * @return array
     */
    public function groupByCategoriesAndWorks(ObjectCollection $products, ObjectCollection $categories,
                                              ObjectCollection $works, ObjectCollection $worksQuantity): array
    {
        $groupedProductsData = [];
        $worksQuantityData = [];
        foreach ($categories as $category) {
            foreach ($works as $work) {
                if ($category->id === $work->category_id) {
                    $groupedProductsData[$category->name][$work->name] = [];
                    $worksQuantityData[$work->name] = $worksQuantity->where('work_id', $work->id)->first()->quantity;
                    foreach ($products as $product) {
                        if ($product->work_id === $work->id) {
                            $groupedProductsData[$category->name][$work->name][$product->id] = $product;
                        }
                    }
                }
            }
        }

        return [$groupedProductsData, $worksQuantityData];
    }

    /**
     * Group products by categories and works with sums
     *
     * @param ObjectCollection $products
     * @param ObjectCollection $categories
     * @param ObjectCollection $works
     * @return array
     */
    public function groupByCategoriesAndWorksWithSums(ObjectCollection $products, ObjectCollection $categories,
                                                      ObjectCollection $works): array
    {
        $groupedProductsData = [];
        $sums = [];
        foreach ($categories as $category) {
            foreach ($works as $work) {
                $price = 0;
                if ($category->id === $work->category_id) {
                    $groupedProductsData[$category->name][$work->name] = [];
                    foreach ($products as $product) {
                        if ($product->work_id === $work->id) {
                            $product->full_price = $work->pivot->quantity;
                            $price += $product->full_price;
                            $groupedProductsData[$category->name][$work->name][$product->id] = $product;
                        }
                    }
                    $sums[$work->name] = number_format($price + $work->full_price, 2);
                }
            }
        }

        return [$groupedProductsData, $sums];
    }

    /**
     * Get plan images
     *
     * @param Collection $products
     * @return ObjectCollection
     */
    public function getPlanImages(Collection $products): ObjectCollection
    {
        $images = collect();
        foreach ($products as $product) {
            if ($product->image) {
                $images->push(
                    [
                        'image_path' => '/images/' . $product->image->image_path,
                        'description' => $product->description
                    ]
                );
            }
        }

        return $images;
    }

    /**
     * Delete plan by ID
     *
     * @param int $id
     * @return bool
     */
    public function deletePlanById(int $id): bool
    {
        return $this->planRepository->deleteById($id);
    }

}
