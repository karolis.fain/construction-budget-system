<?php

namespace App\Services;

use App\Models\Work;
use App\Repositories\ImageRepository;
use App\Repositories\WorkRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;

class WorkService
{
    protected const PRODUCT_IMAGES_DIR = 'images/work_images';

    /**
     * @var WorkRepository
     */
    public WorkRepository $workRepository;

    /**
     * WorkService constructor
     *
     * @param WorkRepository $workRepository
     */
    public function __construct(WorkRepository $workRepository)
    {
        $this->workRepository = $workRepository;
    }

    /**
     * Create new work
     *
     * @param array $attributes
     * @return Work
     */
    public function createWork(array $attributes): Work
    {
        return $this->workRepository->create($attributes);
    }

    /**
     * Update work
     *
     * @param array $attributes
     * @param int $id
     * @return bool
     */
    public function updateWork(array $attributes, int $id): bool
    {
        return $this->workRepository->update($attributes, $id);
    }

    /**
     * Get all works
     *
     * @param string|null $search
     * @return LengthAwarePaginator
     */
    public function getWorks(?string $search = null): LengthAwarePaginator
    {
        if ($search) {
            return $this->workRepository->searchAll($search);
        }

        return $this->workRepository->getAll();
    }

    /**
     * Get all works as collection
     *
     * @return Collection
     */
    public function getWorksCollection(): Collection
    {
        return $this->workRepository->getWorksCollection();
    }

    /**
     * Get work by ID
     *
     * @param int $id
     * @return Work
     */
    public function getWorkById(int $id): Work
    {
        return $this->workRepository->getById($id);
    }

    /**
     * @param int $catId
     * @return Collection
     */
    public function getWorksByCatId(int $catId): Collection
    {
        return $this->workRepository->getByCatId($catId);
    }

    /**
     * @param string $field
     * @param int $value
     * @return Collection
     */
    public function getWorksByCustomField(string $field, int $value): Collection
    {
        return $this->workRepository->getByCustomField($field, $value);
    }

    /**
     * Delete work by ID
     *
     * @param int $id
     * @return bool
     */
    public function deleteWorkById(int $id): bool
    {
        return $this->workRepository->deleteById($id);
    }

}
