<?php

namespace App\Services;

use App\Models\Category;
use App\Repositories\CategoryRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class CategoryService
{
    /**
     * @var CategoryRepository
     */
    public CategoryRepository $categoryRepository;

    /**
     * CategoryService constructor
     *
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Create new category
     *
     * @param array $attributes
     * @return Category
     */
    public function createCategory(array $attributes): Category
    {
        return $this->categoryRepository->create($attributes);
    }

    /**
     * Update category
     *
     * @param array $attributes
     * @param int|null $id
     * @return bool
     */
    public function updateCategory(array $attributes, ?int $id = null): bool
    {
        return $this->categoryRepository->update($attributes, $id);
    }

    /**
     * Get all categories
     *
     * @param string|null $search
     * @return LengthAwarePaginator
     */
    public function getCategories(?string $search = null): LengthAwarePaginator
    {
        if ($search) {
            return $this->categoryRepository->searchAll($search);
        }

        return $this->categoryRepository->getAll();
    }

    /**
     * @return Collection
     */
    public function getCategoriesCollection(): Collection
    {
        return $this->categoryRepository->getCategoriesCollection();
    }

    /**
     * Get category by ID
     *
     * @param int $id
     * @return Category
     */
    public function getCategoryById(int $id): Category
    {
        return $this->categoryRepository->getById($id);
    }

    /**
     * Delete category by ID
     *
     * @param int $id
     * @return bool
     */
    public function deleteCategoryById(int $id): bool
    {
        return $this->categoryRepository->deleteById($id);
    }
}
