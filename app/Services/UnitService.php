<?php

namespace App\Services;

use App\Models\Unit;
use App\Repositories\UnitRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class UnitService
{
    /**
     * @var UnitRepository
     */
    public UnitRepository $unitRepository;

    /**
     * UnitService constructor
     *
     * @param UnitRepository $unitRepository
     */
    public function __construct(UnitRepository $unitRepository)
    {
        $this->unitRepository = $unitRepository;
    }

    /**
     * Create new unit
     *
     * @param array $attributes
     * @return Unit
     */
    public function createUnit(array $attributes): Unit
    {
        return $this->unitRepository->create($attributes);
    }

    /**
     * Update unit
     *
     * @param array $attributes
     * @param int|null $id
     * @return bool
     */
    public function updateUnit(array $attributes, ?int $id = null): bool
    {
        return $this->unitRepository->update($attributes, $id);
    }

    /**
     * Get all categories
     *
     * @param string|null $search
     * @return LengthAwarePaginator
     */
    public function getUnits(?string $search = null): LengthAwarePaginator
    {
        if ($search) {
            return $this->unitRepository->searchAll($search);
        }

        return $this->unitRepository->getAll();
    }

    /**
     * Get all works as collection
     *
     * @return Collection
     */
    public function getUnitsCollection(): Collection
    {
        return $this->unitRepository->getUnitsCollection();
    }

    /**
     * Get unit by ID
     *
     * @param int $id
     * @return Unit
     */
    public function getUnitById(int $id): Unit
    {
        return $this->unitRepository->getById($id);
    }

    /**
     * Delete unit by ID
     *
     * @param int $id
     * @return bool
     */
    public function deleteUnitById(int $id): bool
    {
        return $this->unitRepository->deleteById($id);
    }
}
