<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'code' => 'required|max:15',
            'work_id' => 'required|numeric',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:8192',
            'name' => 'required|string|max:255',
            'description' => 'nullable|max:1000',
            'surcharge' => 'required|numeric',
            'prime_cost' => 'required|numeric',
            'price' => 'required|numeric',
            'price_vat' => 'required|numeric',
            'units_per_square' => 'required|numeric',
            'editable' => 'required|boolean',
            'active' => 'required|boolean'
        ];
    }

}
