<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_name' => [
                'required',
                Rule::unique('plans')->ignore($this->route('id')),
                'max:255'
            ],
            'room_count' => 'required|integer',
            'rooms_squares' => 'required|numeric',
            'bath_squares' => 'required|numeric',
            'quantity' => 'required',
            'prod_quantity' => 'sometimes|required',
            'works' => 'required',
            'products' => 'sometimes|required',
            'sum' => 'required|numeric',
            'sum_vat' => 'required|numeric',
            'square_price' => 'required|numeric'
        ];
    }
}
