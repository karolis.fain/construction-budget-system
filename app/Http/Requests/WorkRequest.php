<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WorkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category_id' => 'required|numeric',
            'unit_id' => 'required|numeric',
            'short_form' => 'required|max:5',
            'name' => ['required',
                Rule::unique('works')->ignore($this->route('id')),
                'max:255'
            ],
            'surcharge' => 'required|numeric',
            'prime_cost' => 'required|numeric',
            'price' => 'required|numeric',
            'price_vat' => 'required|numeric',
            'active' => 'required|boolean'
        ];
    }

}
