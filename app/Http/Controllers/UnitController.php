<?php

namespace App\Http\Controllers;

use App\Http\Requests\UnitRequest;
use App\Models\Unit;
use App\Services\UnitService;
use App\Services\WorkService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class UnitController extends Controller
{
    /**
     * @var UnitService
     */
    protected UnitService $unitService;

    /**
     * @var WorkService
     */
    protected WorkService $workService;

    /**
     * UnitController constructor
     *
     * @param UnitService $unitService
     * @param WorkService $workService
     */
    public function __construct(UnitService $unitService, WorkService $workService)
    {
        $this->unitService = $unitService;
        $this->workService = $workService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|\Illuminate\Contracts\View\View|View
     */
    public function index(Request $request)
    {
        $search = $request->input('search');
        $units = $this->unitService->getUnits($search);

        return view('units/units_list', compact('units'))
            ->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|\Illuminate\Contracts\View\View|Response
     */
    public function create()
    {
        return view('units/create_unit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UnitRequest $request
     * @return RedirectResponse
     */
    public function store(UnitRequest $request)
    {
        $this->unitService->createUnit($request->validated());

        return redirect()->route('unit.index')
            ->with('success', __('Unit created successfully'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|\Illuminate\Contracts\View\View|Response
     */
    public function edit(int $id)
    {
        $unit = $this->unitService->getUnitById($id);

        return view('units/edit_unit', compact('unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UnitRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UnitRequest $request, int $id)
    {
        $this->unitService->updateUnit($request->validated(), $id);

        return redirect()->route('unit.index')
            ->with('success', __('Unit updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        $unit = $this->unitService->getUnitById($id);
        if ($unit->works()->count() === 0) {
            $this->unitService->deleteUnitById($id);

            return redirect()->route('unit.index')
                ->with('success', 'Unit deleted successfully');
        }

        return redirect()->route('unit.index')
            ->with('fail', 'Unit is used in works');
    }

}
