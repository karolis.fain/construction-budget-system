<?php

namespace App\Http\Controllers;

use App\Models\Work;
use App\Services\ProductService;
use App\Services\WorkService;
use Illuminate\Http\Response;

class ApiController extends Controller
{
    /**
     * @var ProductService
     */
    protected ProductService $productService;

    /**
     * @var WorkService
     */
    protected WorkService $workService;

    /**
     * ApiController constructor
     *
     * @param ProductService $productService
     * @param WorkService $workService
     */
    public function __construct(ProductService $productService, WorkService $workService)
    {
        $this->productService = $productService;
        $this->workService =  $workService;
    }

    /**
     * @param int $id
     * @return Response
     */
    public function product(int $id)
    {
        $product = $this->productService->getProductById($id);

        return response()->view('partials/product_add', compact('product'));
    }

    public function category(int $id)
    {
        $works = $this->workService->getWorksByCatId($id);
        if ($works->count() == 0) {
            return response()->json(__('No works in category'), 404);
        }
        $category = $works->first()->category;

        return response()->view('partials/category_add', compact('works', 'category'));
    }

    public function work(int $id)
    {
        $products = $this->productService->getProductsByWorkId($id);
        $work = $this->workService->getWorkById($id);

        return response()->view('partials/work_add', compact('products', 'work'));
    }
}
