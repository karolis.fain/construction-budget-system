<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlanRequest;
use App\Services\CategoryService;
use App\Services\PlanService;
use App\Services\ProductService;
use App\Services\WorkService;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    /**
     * @var CategoryService
     */
    protected CategoryService $categoryService;

    /**
     * @var WorkService
     */
    protected WorkService $workService;

    /**
     * @var PlanService
     */
    protected PlanService $planService;

    /**
     * @var ProductService
     */
    protected ProductService $productService;

    /**
     * ProductController constructor
     *
     * @param CategoryService $categoryService
     * @param PlanService $planService
     * @param WorkService $workService
     * @param ProductService $productService
     */
    public function __construct(CategoryService $categoryService, PlanService $planService,
                                WorkService $workService, ProductService $productService)
    {
        $this->categoryService = $categoryService;
        $this->planService = $planService;
        $this->workService = $workService;
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $search = $request->input('search');
        $plans = $this->planService->getPlans($search);

        return view('plans/plans_list', compact('plans'))
            ->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $categories = $this->categoryService->getCategories();

        return view('plans/create_plan', compact('categories'));
    }

    /**
     * Show form for creating a new resource (with base info)
     *
     * @return View
     */
    public function base(): View
    {
        $plan = $this->planService->getBasePlan();
        $categories = $this->categoryService->getCategoriesCollection();
        $works = $this->workService->getWorksCollection();
        $products = $this->productService->getProductsCollection();

        $groupedWorks = $works->keyBy('name')->groupBy('category.name', true);
        $groupedProducts = $products->keyBy('id')->groupBy('work.name', true);
        $works = $works->keyBy('name');

        $products = $plan->products()->with('work')->get();
        $usedWorks = $plan->works()->with(['category'])->get();
        $worksQuantity = $usedWorks->pluck('pivot');
        $usedCategories = collect($usedWorks->pluck('category'))->unique();

        list($catsWorksProducts, $worksQuantity) = $this->planService
            ->groupByCategoriesAndWorks($products, $usedCategories, $usedWorks, $worksQuantity);

        return view('plans/create_base_plan',
            compact('categories', 'plan', 'catsWorksProducts', 'usedCategories',
                'works', 'groupedWorks', 'groupedProducts', 'worksQuantity'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PlanRequest $request
     * @return RedirectResponse
     */
    public function store(PlanRequest $request): RedirectResponse
    {
        $this->planService->createPlan($request->validated());

        return redirect()->route('plan.index')
            ->with('success', __('Plan created successfully'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        $plan = $this->planService->getPlanById($id);
        $categories = $this->categoryService->getCategoriesCollection();
        $works = $this->workService->getWorksCollection();
        $products = $this->productService->getProductsCollection();

        $groupedWorks = $works->keyBy('name')->groupBy('category.name', true);
        $groupedProducts = $products->keyBy('id')->groupBy('work.name', true);
        $works = $works->keyBy('name');

        $products = $plan->products()->with('work')->get();
        $usedWorks = $plan->works()->with(['category'])->get();
        $worksQuantity = $usedWorks->pluck('pivot');
        $usedCategories = collect($usedWorks->pluck('category'))->unique();


        list($catsWorksProducts, $worksQuantity) = $this->planService
            ->groupByCategoriesAndWorks($products, $usedCategories, $usedWorks, $worksQuantity);

        return view('plans/edit_plan',
            compact('categories', 'plan', 'catsWorksProducts', 'usedCategories',
                'works', 'groupedWorks', 'groupedProducts', 'worksQuantity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PlanRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(PlanRequest $request, int $id): RedirectResponse
    {
        $this->planService->updatePlan($request->validated(), $id);

        return redirect()->route('plan.index')
            ->with('success', __('Plan updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $this->planService->deletePlanById($id);

        return redirect()->route('plan.index')
            ->with('success', 'Plan deleted successfully');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function export(int $id)
    {
        $plan = $this->planService->getPlanById($id);
        $works = $plan->works()->get()->keyBy('name');

        $products = $plan->products()->with('work')->get();
        $usedWorks = $plan->works()->with('category')->get();
        $usedCategories = collect($usedWorks->pluck('category'))->unique();

        list($catsWorksProducts, $workSums) = $this->planService
            ->groupByCategoriesAndWorksWithSums($products, $usedCategories, $usedWorks);

        $images = $this->planService->getPlanImages($products);

        return view('export', compact(
            'plan', 'works', 'catsWorksProducts', 'images', 'workSums'
        ));

        $pdf = PDF::setOptions(['isRemoteEnabled' => true])->loadView('export', compact(
            'plan', 'works', 'catsWorksProducts', 'images', 'workSums'
        ));

        return $pdf->stream('invoice.pdf');
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function replicate(int $id): RedirectResponse
    {
        $plan = $this->planService->getPlanById($id);

        $newPlan = $plan->replicate();
        $newPlan->project_name .= '(Klonas)';
        $newPlan->save();

        foreach ($plan->products as $products) {
            $newPlan->products()->attach($products, ['quantity' => $products->pivot->quantity]);
        }
        foreach ($plan->works as $work) {
            $newPlan->works()->attach($work, ['quantity' => $work->pivot->quantity]);
        }

        return redirect()->route('plan.index')
            ->with('success', 'Plan cloned successfully');
    }
}
