<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Services\ProductService;
use App\Services\UnitService;
use App\Services\WorkService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;

class ProductController extends Controller
{
    /**
     * @var ProductService
     */
    protected ProductService $productService;

    /**
     * @var WorkService
     */
    protected WorkService $workService;

    /**
     * @var UnitService
     */
    protected UnitService $unitService;

    /**
     * ProductController constructor
     *
     * @param ProductService $productService
     * @param WorkService $workService
     * @param UnitService $unitService
     */
    public function __construct(ProductService $productService, WorkService $workService,
                                UnitService $unitService)
    {
        $this->productService = $productService;
        $this->workService = $workService;
        $this->unitService = $unitService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $search = $request->input('search');
        $products = $this->productService->getProducts($search);

        return view('products/products_list', compact('products'))
            ->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $works = $this->workService->getWorksCollection();

        return view('products/create_product',
            compact('works'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest $request
     * @return RedirectResponse
     */
    public function store(ProductRequest $request)
    {
        $this->productService->createProduct($request->validated());

        return redirect()->route('prd.index')
            ->with('success', __('Product created successfully'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id)
    {
        $product = $this->productService->getProductById($id);
        $works = $this->workService->getWorksCollection();

        return view('products/edit_product',
            compact('product', 'works'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductRequest $request
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(ProductRequest $request, int $id)
    {
        $this->productService->updateProduct($request->validated(), $id);

        return redirect()->route('prd.index')
            ->with('success', __('Product updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        $product = $this->productService->getProductById($id);
        if ($product->plans()->count() === 0) {
            $this->productService->deleteProductById($id);

            return redirect()->route('prd.index')
                ->with('success', 'Product deleted successfully');
        }

        return redirect()->route('prd.index')
            ->with('fail', 'Product is used in plans');
    }

}
