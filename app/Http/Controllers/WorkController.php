<?php

namespace App\Http\Controllers;

use App\Http\Requests\WorkRequest;
use App\Models\Work;
use App\Services\CategoryService;
use App\Services\UnitService;
use App\Services\WorkService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Config;

class WorkController extends Controller
{
    /**
     * @var WorkService
     */
    protected WorkService $workService;

    /**
     * @var CategoryService
     */
    protected CategoryService $categoryService;

    /**
     * @var UnitService
     */
    protected UnitService $unitService;

    /**
     * ProductController constructor
     *
     * @param WorkService $workService
     * @param CategoryService $categoryService
     * @param UnitService $unitService
     */
    public function __construct(WorkService $workService, CategoryService $categoryService,
                                UnitService $unitService)
    {
        $this->workService = $workService;
        $this->categoryService = $categoryService;
        $this->unitService = $unitService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $search = $request->input('search');
        $works = $this->workService->getWorks($search);

        return view('works/works_list', compact('works'))
            ->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $categories = $this->categoryService->getCategoriesCollection();
        $units = $this->unitService->getUnitsCollection();
        $types = Config::get('constants.types');

        return view('works/create_work',
            compact('categories', 'units', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param WorkRequest $request
     * @return RedirectResponse
     */
    public function store(WorkRequest $request)
    {
        $this->workService->createWork($request->validated());

        return redirect()->route('work.index')
            ->with('success', __('Work created successfully'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id)
    {
        $work = $this->workService->getWorkById($id);
        $categories = $this->categoryService->getCategoriesCollection();
        $units = $this->unitService->getUnitsCollection();

        return view('works/edit_work',
            compact('work', 'categories', 'units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param WorkRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(WorkRequest $request, int $id)
    {
        $this->workService->updateWork($request->validated(), $id);

        return redirect()->route('work.index')
            ->with('success', __('Work updated successfully'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id)
    {
        $work = $this->workService->getWorkById($id);
        if ($work->plans()->count() === 0) {
            $this->workService->deleteWorkById($id);

            return redirect()->route('work.index')
                ->with('success', __('Work deleted successfully'));
        }

        return redirect()->route('work.index')
            ->with('fail', __('Work is used in plans'));
    }

}
