$(document).ready(function() {
    let select = document.getElementById('work_id');
    let text = select.options[select.selectedIndex].text;
    $('#code').val(text.substring(0, text.indexOf(' -')));

    $('#work_id').on('change', function () {
        let text = this.options[this.selectedIndex].text;
        $('#code').val(text.substring(0, text.indexOf(' -')));
    });

    $('#prime_cost').on('change', function () {
        $('#price').val(this.value);
        $('#price_vat').val(this.value);
    });
});
