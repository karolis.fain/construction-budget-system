$(document).ready(function() {
    $(window).keydown(function(event){
        if(event.keyCode === 13) {
            event.preventDefault();
            return false;
        }
    });

    let rows = $('tr.row-work');
    if (rows.length > 0) {
        rows.each(function () {
            updateWorksAndProductsPrices($(this));
        });
    }

    $('#add-category').on('change', function () {
        let id = $(this).val();
        $.ajax({
            url: "/api/categories/" + id,
            type: 'GET',
            success: function(result) {
                insertAt('#plan-table-content', result);
                removeCategorySelect();
            },
            error: function (result) {
                removeCategorySelect();
                let alert = $('#alert-warning');
                alert.text(result.responseJSON);
                alertEmpty(alert);
            }
        });
    });

    $('#rooms_squares, #bath_squares').on('change', function () {
        updateSumAndSquarePrice();
    });
});

$(document).on({
    mouseenter: function () {
        $(this).find('img').show();
    },
    mouseleave: function () {
        $(this).find('img').hide();
    }
}, '.image-pop');

//Product part
$(document).on('click', '.delete-product', function () {
    let tr = $(this).closest('tr');
    enableRemovedProductSelect(tr);
    tr.remove();
    updateSumAndSquarePrice();
});

$(document).on('change', '.product', function () {
    let select = $(this);
    let id = select.val();
    let tr = select.closest('tr');
    $.ajax({
        url: "/api/products/" + id,
        type: 'GET',
        success: function(result) {
            let newRow = $(result);
            insertBeforeItem(select, newRow);
            updateWorksAndProductsPrices(tr.prevAll('tr.row-work:first'));
            updateSumAndSquarePrice();
            removeProductSelect(select);
        }
    });
});

function removeProductSelect(item) {
    item.find("option:selected").prop("disabled", true);
    let enabled = item.find('option:not(:disabled)');
    if (enabled.length === 0) {
        item.closest('tr').hide();
    }
}

function enableRemovedProductSelect(trTag) {
    let productId = trTag.find('[name^=products]').val();

    let option = $('.product-select option[value="' + productId + '"]');
    option.prop("disabled", false);

    let selectTrTag = option.closest('tr');
    let enabled = selectTrTag.find('option:not(:disabled)');
    if (enabled.length > 0) {
        selectTrTag.show();
    }
}

//Category part
$(document).on('click', '.delete-category', function () {
    let id = this.id;
    $(this).closest('tr').nextUntil('.category-row').addBack().remove();
    enableRemovedCategorySelect(id);
    updateSumAndSquarePrice();
});

function removeCategorySelect() {
    $("#add-category option:selected").attr('disabled', 'disabled');
    let enabled = $('#add-category option:not(:disabled)');
    if (enabled.length === 0) {
        $('#plan-table-content tr:last').hide();
    }
}

function enableRemovedCategorySelect(id) {
    let option = $('.category-row option[value="' + id + '"]');
    option.prop("disabled", false);

    let selectTrTag = option.closest('tr');
    let enabled = selectTrTag.find('option:not(:disabled)');
    if (enabled.length > 0) {
        selectTrTag.show();
    }
}

//Work
$(document).on('change', '.work', function () {
    let select = $(this);
    let id = select.val();
    $.ajax({
        url: "/api/works/" + id,
        type: 'GET',
        success: function(result) {
            let newRow = $(result);
            updateWorksAndProductsPrices(newRow);
            insertBeforeItem(select, newRow);
            removeWorkSelect(select);
            updateSumAndSquarePrice();
        },
        error: function (result) {
            removeWorkSelect(select);
            let alert = $('#alert-warning');
            alert.text(result.responseJSON);
            alertEmpty(alert);
        }
    });
});

$(document).on('click', '.delete-work', function () {
    let trTag = $(this).closest('tr');
    enableRemovedWorkSelect(trTag);
    trTag.nextUntil('.row-work, .work-select').addBack().remove();
    updateSumAndSquarePrice();
});

$(document).on('change', '.row-work .quantity', function () {
    let trTag = $(this).closest('tr');
    updateWorksAndProductsPrices(trTag);
    updateSumAndSquarePrice();
});

$(document).on('change', '.row-product .quantity', function () {
    let trTag = $(this).closest('tr').prevAll('.row-work').first();
    updateWorksAndProductsPrices(trTag);
    updateSumAndSquarePrice();
});

function removeWorkSelect(item) {
    item.find("option:selected").prop("disabled", true);
    let enabled = item.find('option:not(:disabled)');
    if (enabled.length === 0) {
        item.closest('tr').hide();
    }
}

function enableRemovedWorkSelect(trTag) {
    let workId = trTag.find('[name^=works]').val();

    let option = $('.work-select option[value="' + workId + '"]');
    option.prop("disabled", false);

    let selectTrTag = option.closest('tr');
    let enabled = selectTrTag.find('option:not(:disabled)');
    if (enabled.length > 0) {
        selectTrTag.show();
    }
}

//Row inserts
function insertAt(selector, newData) {
    $(selector).find('tr:last').before(newData);
}

function insertBeforeItem(item, newData) {
    item.closest('tr').before(newData);
}

//Sum and price calculations
function updateWorksAndProductsPrices(newRow) {
    let count = newRow.find('.quantity').val();
    let workSum = 0;

    newRow.nextUntil('.product-select, .work-select, .category-row').each(function () {
        let row = $(this);
        let unitPrice = row.find('.unit-price').text().slice(0, -1);
        let unitTag = row.find('.units-per');

        let unitsCount;
        if (unitTag.is('span')) {
            unitsCount = row.find('input[name=units]').val() * count;
        } else if (!unitTag.val()) {
            unitsCount = row.find('input[name=units]').val();
        } else {
            unitsCount = unitTag.val();
        }

        let price = unitPrice * unitsCount;
        workSum += +(price.toFixed(2));

        unitsCount = Number.isInteger(unitsCount) ? unitsCount : Number(unitsCount).toFixed(2);
        if (unitTag.is('span')) {
            unitTag.text(unitsCount);
        } else {
            unitTag.val(unitsCount);
        }
        row.find('.value').text(price !== 0 ? price.toFixed(2) + '€' : price + '€');
    });
    let workPrice = count * newRow.find('.work-price').text().slice(0, -1);
    workSum += +workPrice;

    newRow.find('.value').text(workPrice !== 0 ? workPrice.toFixed(2) + '€': workPrice + '€');
    newRow.find('.sum').text(workSum !== 0 ? workSum.toFixed(2) + '€' : workSum + '€');
}

function updateSumAndSquarePrice() {
    let totalSum = 0;
    $('tr.row-work .sum').each(function () {
        totalSum += +(+$(this).text().slice(0, -1)).toFixed(2);
    });

    let roomsSquares = $('#rooms_squares').val();
    let bathSquares = $('#bath_squares').val();

    if (roomsSquares === '') {
        roomsSquares = 0;
    }
    if (bathSquares === '') {
        bathSquares = 0;
    }

    let totalSumVat = Number(totalSum * 1.21);
    let commonSquares = +roomsSquares + +bathSquares;
    let squarePrice = commonSquares !== 0 ? totalSumVat / (+roomsSquares + +bathSquares) : 0;

    $('[name=sum]').val(Number(totalSum).toFixed(2));
    $('[name=sum_vat]').val(totalSumVat.toFixed(2));
    $('[name=square_price]').val(squarePrice.toFixed(2));
}

function alertEmpty(alert) {
    alert.toggle();
    setTimeout(function () {
        alert.toggle();
    }, 5000);
}
