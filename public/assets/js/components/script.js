function alertHide() {
    let success = document.getElementById('alert-success');
    let fail = document.getElementById('alert-warning');
    if (success || fail) {
        setTimeout(function () {
            let alert = success || fail;
            let fadeEffect = setInterval(function () {
                if (!alert.style.opacity) {
                    alert.style.opacity = 1;
                }
                if (alert.style.opacity > 0) {
                    alert.style.opacity -= 0.1;
                } else {
                    clearInterval(fadeEffect);
                    alert.style.display = 'none';
                }
            }, 200);
        }, 5000);
    }
}

window.onload = alertHide;
