<?php

use App\Http\Controllers\ApiController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\WorkController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'] , function () {

    Route::group(['prefix' => 'categories'], function () {

        Route::get('/', [CategoryController::class, 'index'])->name('cat.index');

        Route::get('/create', [CategoryController::class, 'create'])->name('cat.create');
        Route::post('/create', [CategoryController::class, 'store'])->name('cat.store');

        Route::get('/{id}', [CategoryController::class, 'show'])->name('cat.show');

        Route::get('/{id}/edit', [CategoryController::class, 'edit'])->name('cat.edit');
        Route::put('/{id}/update', [CategoryController::class, 'update'])->name('cat.update');

        Route::delete('/{id}', [CategoryController::class, 'destroy'])->name('cat.destroy');
    });

    Route::group(['prefix' => 'products'], function () {

        Route::get('/', [ProductController::class, 'index'])->name('prd.index');

        Route::get('/create', [ProductController::class, 'create'])->name('prd.create');
        Route::post('/create', [ProductController::class, 'store'])->name('prd.store');

        Route::get('/{id}', [ProductController::class, 'show'])->name('prd.show');

        Route::get('/{id}/edit', [ProductController::class, 'edit'])->name('prd.edit');
        Route::put('/{id}/update', [ProductController::class, 'update'])->name('prd.update');

        Route::delete('/{id}', [ProductController::class, 'destroy'])->name('prd.destroy');
    });

    Route::group(['prefix' => 'plans'], function () {

        Route::get('/', [PlanController::class, 'index'])->name('plan.index');

        Route::get('/create', [PlanController::class, 'create'])->name('plan.create');
        Route::get('/base', [PlanController::class, 'base'])->name('plan.base');
        Route::post('/create', [PlanController::class, 'store'])->name('plan.store');

        Route::get('/{id}', [PlanController::class, 'show'])->name('plan.show');

        Route::get('/{id}/edit', [PlanController::class, 'edit'])->name('plan.edit');
        Route::put('/{id}/update', [PlanController::class, 'update'])->name('plan.update');

        Route::get('/{id}/export', [PlanController::class, 'export'])->name('plan.export');
        Route::get('/{id}/clone', [PlanController::class, 'replicate'])->name('plan.replicate');

        Route::delete('/{id}', [PlanController::class, 'destroy'])->name('plan.destroy');
    });

    Route::group(['prefix' => 'units'], function () {

        Route::get('/', [UnitController::class, 'index'])->name('unit.index');

        Route::get('/create', [UnitController::class, 'create'])->name('unit.create');
        Route::post('/create', [UnitController::class, 'store'])->name('unit.store');

        Route::get('/{id}', [UnitController::class, 'show'])->name('unit.show');

        Route::get('/{id}/edit', [UnitController::class, 'edit'])->name('unit.edit');
        Route::put('/{id}/update', [UnitController::class, 'update'])->name('unit.update');

        Route::delete('/{id}', [UnitController::class, 'destroy'])->name('unit.destroy');
    });

    Route::group(['prefix' => 'works'], function () {

        Route::get('/', [WorkController::class, 'index'])->name('work.index');

        Route::get('/create', [WorkController::class, 'create'])->name('work.create');
        Route::post('/create', [WorkController::class, 'store'])->name('work.store');

        Route::get('/{id}', [WorkController::class, 'show'])->name('work.show');

        Route::get('/{id}/edit', [WorkController::class, 'edit'])->name('work.edit');
        Route::put('/{id}/update', [WorkController::class, 'update'])->name('work.update');

        Route::delete('/{id}', [WorkController::class, 'destroy'])->name('work.destroy');
    });

    Route::group(['prefix' => 'api'], function () {
        Route::get('/categories/{id}', [ApiController::class, 'category']);
        Route::get('/products/{id}', [ApiController::class, 'product']);
        Route::get('/works/{id}', [ApiController::class, 'work']);
    });
});

Route::any('/{any}', function () {
    return redirect('/plans');
})->where('any', '.*');
